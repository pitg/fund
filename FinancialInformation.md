# Financial Information


## Recipient Information

- Name:
- Address:
- Email:


## Bank Information

- Name:
- Address:
- SWIFT Code:
- IBAN / CLABE / Account Number:
- Account Currency (must receive USD): *Yes* / *No*

If an Intermediary Bank is required, please indicate:

- SWIFT or ABA Code from Intermediary Bank:
- Name:
- Country:
- Intermediary Account Number (between banks):
 



**Signature**
**Position**
**Date**
