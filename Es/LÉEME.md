# Fondo PITG

Este es el espacio para organizar el Fondo PITG.

## Voluntariado 

La [política de financiación](https://gitlab.com/pitg/fund/-/blob/master/Es/PoliticaFinanciacion.md) y las decisiones de financiación son asignadas por un grupo voluntario. Para participar del voluntariado, únase al Fondo PITG [lista de correo del subgrupo](https://lists.mayfirst.org/mailman/listinfo/pitg-fund).

## Postule

La postulación debe hacerse en inglés. Para solicitar el fondo de viaje, envíe un correo electrónico a pitg-fund@lists.mayfirst.org e incluya la siguiente información:

 * Información de la persona destinataria (nombre, organización, correo electrónico, teléfono, dirección);
 * Información del evento (IETF / W3C / ITU / IEEE/3GPP / ICANN / et al, y fecha);
 * ¿Está o no pre-aprobada/o o acreditada/o para participar?;
 * Asistencia financiera requerida (inscripción, boleto de tren/avión, alojamiento, taxis, tarifas de visa, seguro de viaje, comidas, cuidado infantil; y monto total)

El grupo del Fondo PITG confirmará la recepción de su correo electrónico.
Dentro de las dos semanas posteriores al envío de toda la información mencionada anteriormente, el grupo del Fondo responderá para aprobar o rechazar su solicitud.

## Documentos requeridos (para referencia)

Una vez que su solicitud haya sido aprobada, debe:

* Enviar su [información financiera](https://gitlab.com/pitg/fund/-/blob/master/FinancialInformation.md) (en inglés).

* Cumplir con el [Código de Conducta de PITG](https://gitlab.com/pitg/code-of-conduct/-/blob/master/code-of-conduct.md) (en inglés).

* Aceptar y firmar un [Memorando de Entendimiento (MOU)] (https://gitlab.com/pitg/fund/-/blob/master/MOU.md) para quienes reciben los fondos (en inglés).

## Otras fuentes de financiación para viajes

 * [Fondo de viajes de IRTF (en inglés)](https://irtf.org/travelgrants)
 * [Fondo de viajes del IETF LLC (en inglés)]()
 * [ISOC]()
 * [Guía de fuentes alternativas de financiación del Open Technology Fund (en inglés)](https://guide.opentech.fund/appendix-iv-alternative-sources-of-support)
 * [Página de fuentes de financiación para viajes en la wiki Geek Feminism (en inglés)](https://geekfeminism.fandom.com/wiki/Travel_funding)
