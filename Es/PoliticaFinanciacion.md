# Política de financiación de PITG

## Información general

El fondo de viajes del PITG apoya la participación del interés público en la comunidad técnica y la participación de profesionales en tecnólogía, en la gobernanza de Internet. Desde 2019, el Grupo de Tecnología de Interés Público (PITG, por sus siglas en inglés) ha estado trabajando con diferentes organizaciones para crear un marco en que los fondos disponibles se puedan utilizar para patrocinar la asistencia al IETF de personas de grupos subrepresentados, especialmente aquellas que trabajan en tecnologías de interés público, con el fin de contribuir a una participación más diversa e inclusiva en foros como el IETF. Lanzado en 2022, en un esfuerzo conjunto entre el Equipo Digital de ARTÍCULO 19 y Derechos Digitales (la organización “Anfitriona”), el Fondo otorga apoyos financieros. Quienes integran el subgrupo del Fondo PITG son voluntarias/os y la Fundación Ford es la "institución financiadora". Derechos Digitales como "Anfitriona" en representación del subgrupo de Fondos PITG, gestiona la administración del apoyo del Fondo PITG.

### Consideraciones

 * Un/a integrante del personal de la organización Anfitriona está suscrita/o a la lista del subgrupo de Fondos PITG.
 * Una persona solicitante que también es voluntaria del subgrupo PITG debe recusarse de la toma de decisiones sobre su solicitud.

### Los objetivos del Fondo son los siguientes:
    * Promover el trabajo de tecnología de interés público en organismos clave de gobernanza y estándares de internet.
    * Cerrar la brecha de conocimiento dentro de las comunidades técnicas de gobernanza de internet, otras organizaciones de Desarrollo de Estándares e integrantes de la Sociedad Civil.
    * Cultivar la participación sostenida y efectiva de representantes de la sociedad civil o profesionales de la tecnología de interés público en la gobernanza de internet y los organismos de estandarización.
    * Aumentar la diversidad de la participación en estos órganos.

## Detalles relevantes para incluir en una postulación

La siguiente información es relevante para que el subgrupo de Fondos PITG, a través de la organización Anfitriona, evalúe la financiación de la persona destinataria.

### Información para la inscripción
Nombre:
Organización:
Correo:
Teléfono:
Dirección:
Datos para la transferencia de fondos:

### Información del evento
Especifique IETF, W3C, ITU, IEEE,3GPP, ICANN, etc.: ______ 
Si es necesaria una aprobación previa para unirse (en el caso del W3C o similar), ¿ya se ha otorgado?: (S/N)____
Fecha del evento:

### Tipo de asistencia requerida (se puede incluir cualquiera)
     Cuota de inscripción 
     Pasaje
     Alojamiento
     Traslados al aeropuerto
     Transporte local
     Tasas de visado
     Seguro de viaje
     Comida
     Cuidado infantil

### Cantidad total solicitada

### Detalles opcionales que las/os voluntarias/os del subgrupo del Fondo PITG pueden solicitar

Las razones, habilidades o experiencia que usted y/o su organización tienen para justificar su participación en el evento indicado, y la asignación de recursos solicitada

## Condiciones

Las/os solicitantes y beneficiarias/os de los premios deben tener en cuenta lo siguiente: i) la asistencia solicitada puede ser aprobada total o parcialmente; ii) cualquier incidente, tarifa o costo asociado con cambios en las reservas debido a problemas personales que excedan la asistencia otorgada no será cubierto por el proyecto; iii) no se otorgarán pagos en efectivo o en el sitio; iv) dentro de un período de 15 días a partir de la ejecución de la actividad, se le pedirá que presente un informe a las/los  integrantes del subgrupo del Fondo PITG; v) se puede solicitar la devolución de fondos si no ejecuta la actividad para la cual los solicitó.

Las/os solicitantes y las/os destinatarias/os de los premios deben leer y aceptar comportarse de acuerdo con el [Código de Conducta de PITG](https://gitlab.com/pitg/code-of-conduct) (En inglés).

## Proceso

1. La/el solicitante envía su solicitud de asistencia al subgrupo del Fondo PITG en un correo electrónico que incluye los detalles relevantes.
2. La lista del subgrupo se utiliza para discutir si se otorga o no la financiación, y cualquier integrante puede responder a la solicitud para obtener más detalles sobre la inscripción.
3. Dentro de dos semanas, se necesita una aprobación de una persona voluntaria del subgrupo del Fondo PITG, y no hay lugar a objeciones.
4. La organización Anfitriona notifica a la persona solicitante de la aprobación o no.
5. La organización Anfitriona envía a la persona solicitante el documento transaccional necesario para las firmas y los datos bancarios.
6. Cada mes, el subgrupo del Fondo PITG informa al PITG sobre la cantidad de fondos que se han asignado, a quién y para qué.
