# PITG Funding Policy

## General Information

The PITG travel fund supports public interest participation in the technical community and technologists' particiption in internet governance. Since 2019 the Public Interest Technology Group has been working with different organizations to create a framework where available funding can be used to sponsor the attendance at the IETF of individuals from under-represented groups, especially those working in public interest technologies, in order to contribute to a more diverse and inclusive participation in fora such as the IETF. Launched in 2022 the fund gives financial awards in a joint endeavor between ARTICLE 19’s Team Digital and Derechos Digitales (“The Host”). Members of the PITG Fund subgroup are volunteers and Ford Foundation is the “funding institution”. Derechos Digitales as the Host, on behalf of the PITG Fund subgroup, manages the administration of the PITG Fund financial award to the Recipient.

### Considerations

 * A representative staff member of The Host is subscribed to the PITG Fund subgroup list.
 * An applicant that is also a volunteer of the PITG subgroup must recuse themselves from decision making about their application.

### The Fund goals are the following:
    • To promote public interest technology work in key Internet governance and standards bodies.
    • To bridge the knowledge gap within technical Internet Governance communities, other Standards Development organizations (SDOs), and members of Civil Society (CSOs).
    • To cultivate sustained and effective participation of civil society representatives and/or public interest technologists in Internet governance and standards bodies.
    • To increase the diversity of representation in these bodies.

## Relevant details to include in an application

The following information is relevant to the PITG Fund subgroup to evaluate funding the Recipient with an award through the Host.

### Applicant information
Name:
Organization:
Email:
Phone:
Address:
Funds transfer details:

### Event information
Specify IETF, W3C, ITU, IEEE, 3GPP, ICANN, etc.: ______ 
If pre-approval for joining is necessary (in the case of W3C or such), has this already been granted?: (Y/N)____
Event date:

### Type of assistance required (any can be included)
     Registration fee 
     Airfare
     Accommodation
     Airport transfers
     Local transportation
     Visa fees
     Travel insurance
     Meals
     Child care

Please go through the Memorandum of Understanding (MoU) for details on the extent of support the PITG Fund can provide. 

### Total amount requested

### Optional details that PITG Fund subgroup volunteers might ask

The reasons, skills or experience that you and/or your organization have to justify your participation in the indicated event and the requested allocation of resources.

## Conditions

Applicants and award recipients must please notice the following: i) the requested assistance may be approved in full or partially; ii) any incidentals, fees or costs associated with changes in reservations due to personal issues exceeding the awarded assistance will not be covered by the project; iii) no cash or onsite payments will be given; iv) within a period of 15 days from the execution of the activity, you will be asked to submit narrative report to the members of PITG-fund; v) the return of funds may be requested if you do not execute the activity for which you requested funds.

Applicants and award recipients must please read and agree to behave in accordance with [PITG Code of Conduct](https://gitlab.com/pitg/code-of-conduct).

## Process

1. Applicant sends their request to the PITG subgroup for assistance in an email that includes the relevant details.
2. The PITG Fund subgroup list is used for discussion about whether or not to award the funding, and any member can reply to the request for more details from the applicant.
3. Within two weeks one approval from a PITG Fund subgroup volunteer is needed and there are no objections.
4. The Host notifies the applicant of approval or not.
5. The Host sends the Applicant the necessary transactional document for signatures and banking details.
6. Each month the PITG Fund reports to the PITG about how much funding has been allocated and to whom and for what.
