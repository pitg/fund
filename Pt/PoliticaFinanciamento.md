# Política de financiamento PITG

## Informações gerais

O fundo de viagens PITG apóia a participação do interesse público na comunidade técnica e a participação dos tecnólogos na governança da Internet. O Grupo de Tecnologia de Interesse Público (PITG, por sua sigla em inglês) atua desde 2019 com diferentes organizações para a criação de uma estrutura onde o financiamento disponível possa ser utilizado no apoio à participação de pessoas de grupos sub-representados na IETF. Em especial aquelas que trabalham com tecnologias de interesse público, a fim de contribuir para uma participação mais diversa e inclusiva em fóruns como o IETF. Lançado em 2022, o Fundo concede prêmios financeiros através de um esforço conjunto entre a Equipe Digital da ARTIGO 19 e a Derechos Digitales ("A Anfitriã"). Quem integra o subgrupo do Fundo PITG se voluntariou e a Fundação Ford se colocou como a "instituição financiadora". A administração da recepção do prêmio financeiro do Fundo PITG é feita pela Derechos Digitales, a Anfitriã, em nome do subgrupo do Fundo. 

### Considerações

 * Uma pessoa da equipe Anfitriã está inscrita na lista de subgrupos do Fundo PITG;
 * Uma pessoa inscrita no Fundo será voluntária no subgrupo PITG, porém ela deve se recusar a tomar decisões sobre sua candidatura.

### Os objetivos do Fundo são:
    • Fomentar trabalhos com tecnologias de interesse público nos principais órgãos de governança e normas da internet;
    • Reduzir as lacunas de conhecimento nas comunidades técnicas de governança da internet, Organizações de desenvolvimento de padrões e integrantes da sociedade civil;
    • Cultivar a participação sustentada e efetiva de representantes da sociedade civil e profissionais da tecnologia de interesse público na governança da internet e nos órgãos de padronização;
    • Ampliar a diversidade de representação nesses órgãos.

## Detalhes relevantes para serem incluídos em uma inscrição

As informações a seguir são fundamentais para que o subgrupo do Fundo PITG, através da Anfitriã, avalie se financiará a pessoa inscrita.

### Informações para as inscrições
Nome:
Organização:
E-mail:
Telefone:
Endereço:
Dados para a transferência de fundos:

### Informações do evento:
Especificar IETF, W3C, ITU, IEEE, 3GPP, ICANN, etc.: ______ 
Caso seja necessária a pré-aprovação para a adesão (no caso do W3C ou similar), ela já foi concedida? (S/N)____
Data do evento:

### Tipo de auxílio solicitado (qualquer um pode ser incluído)
Taxa de inscrição 
Tarifa aérea
Hospedagem
Traslados para o aeroporto
Transporte local
Taxas de visto
Seguro viagem
Refeições
Cuidado infantil

### Montante total solicitado

### Detalhes opcionais que podem ser requisitados pelas pessoas voluntárias do subgrupo do Fundo PITG

Os motivos, habilidades e experiências que você e/ou sua organização têm que justifiquem a sua participação no evento indicado e a alocação de recursos solicitada.

## Condições

Quem se inscrever e receber o financiamento deve observar o seguinte: i) o auxílio solicitado pode ser aprovado de forma integral ou parcial; ii) quaisquer incidentes, taxas ou custos associados a mudanças nas reservas devido a questões pessoais que excedam o auxílio concedido não serão cobertos pelo projeto; iii) nenhum pagamento em dinheiro ou no local será realizado; iv) quinze dias após o início da atividade, será pedido que você apresente um relatório narrativo ao Fundo PITG; v) a devolução dos fundos pode ser solicitada se você não efetivar a atividade para a qual solicitou o auxílio.

Quem se inscrever e receber o financiamento deve ler e estar de acordo com o [Código de Conduta PITG](https://gitlab.com/pitg/code-of-conduct) (em inglês).

## Processo

1. A inscrição para o pedido do auxílio incluindo os detalhes relevantes é enviada em um e-mail para o subgrupo PITG;
2. A lista do subgrupo do Fundo PITG é utilizada para discussão sobre a concessão ou não do financiamento; qualquer integrante pode responder à solicitação para obter mais detalhes de quem se inscreveu;
3. No prazo de duas semanas é necessária uma aprovação de uma pessoa voluntária do subgrupo do Fundo PITG, e não há objeções;
4. A Anfitriã informa à pessoa inscrita se foi aprovada ou não;
5. A Anfitriã envia a quem se inscreveu os documentos transacionais necessários para as assinaturas e detalhes bancários; 
6. Todos os meses o Fundo PITG informa ao PITG quanto dinheiro foi alocado, a quem e para qual finalidade.
