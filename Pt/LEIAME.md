# Fundo PITG

Este é o espaço para a organização do Fundo PITG.

## Voluntariado

Um grupo voluntário define e toma decisões sobre a [política de financiamento](https://gitlab.com/pitg/fund/-/blob/master/Pt/PoliticaFinanciamento.md). Junte-se ao Fundo PITG para se voluntariar [lista de e-mails do subgrupo] (https://lists.mayfirst.org/mailman/listinfo/pitg-fund).

## Como aplicar 

A aplicação deve ser feita em inglês. para solicitar ao Fundo de Viagens, envie um e-mail para pitg-fund@lists.mayfirst.org e inclua as informações a seguir:

 * Informações pessoais (nome, organização, e-mail, telefone, endereço);
 * Informações do evento (IETF/W3C/ITU/IEEE/3GPP/ICANN/etc., data);
 * Foi ou não pré-aprovada/o e habilitada/o para participar?;
 * Assistência financeira necessária (inscrição, trem/tarifa aérea, hospedagem, táxis, taxas de visto, seguro viagem, refeições, cuidado infantil) e montante total.

O grupo do Fundo PITG confirmará o recebimento do seu e-mail.
Duas semanas após o envio das informações listadas acima, você receberá a resposta do grupo do Fundo com a aprovação ou a recusa da sua solicitação.

## Documentos exigidos (para referências)

Assim que a sua solicitação tiver sido aprovada, será necessário:

* Enviar suas [informações financeiras](https://gitlab.com/pitg/fund/-/blob/master/FinancialInformation.md) (em inglês);

* Cumprir o [Código de Conduta PITG](https://gitlab.com/pitg/code-of-conduct/-/blob/master/code-of-conduct.md) (em inglês);

* Aceitar e assinar um [Memorando de Entendimento (MOU)](https://gitlab.com/pitg/fund/-/blob/master/MOU.md) (em inglês) para quem recebe os fundos.

## Outras fontes de financiamento para viagens

 * [Fundo para viagens de IRTF (em inglês)](https://irtf.org/travelgrants)
 * [Fundo para viagens de IETF LLC (em inglês)]()
 * [ISOC]()
 * [Guia do Open Technology Fund para fontes alternativas de financiamento (em inglês)](https://guide.opentech.fund/appendix-iv-alternative-sources-of-support)
 * [Página de financiamento de viagens na Wiki do Geek Feminism (em inglês)](https://geekfeminism.fandom.com/wiki/Travel_funding)
