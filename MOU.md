# Memorandum of Understanding for Recipients
(Hereinafter referred to as the “MOU”)

Between: Host Organization - Derechos Digitales on behalf of the PITG-Fund sub-group

and: 2022 Travel-Fund Recipient

- Name:
- Email:
- Phone:
- Location:

(referred to as “the Recipient”)

(collectively referred to as “the parties”)

## 1. Introduction

1.1 This MOU signed between the Host Organization and the Recipient defines the roles and responsibilities of each party in relation to the Travel-Fund (hereinafter “the Fund-award”).

1.2 Travel-Fund Background: Since 2019, the Public Interest Technology Group has been working with different organizations to create a framework were available funding can be used to sponsor the attendance at the IETF of individuals from under-represented groups, specially those working in public interest technologies, in order to contribute to a more diverse and inclusive participation in fora such as the IETF. Launched in 2021, the Fund-award is a joint endeavour of ARTICLE 19’s team Digital, Derechos Digitales, members of the PITG-Fund subgroup, and Ford Foundation (Ford, hereinafter the “funding institution”). Derechos Digitales, on behalf of the PITG-Fund sub-group manages the PITG-Fund. The Host is responsible for managing the award to the Recipient according this MOU.

1.3 Fund-award Goals:

* To promote public interest technology work in key Internet governance and standards bodies.
* To bridge the knowledge gap within technical Internet Governance communities, other Standards Development organizations (SDOs), and members of Civil Society (CSOs).
* To cultivate sustained and effective participation of civil society representatives and/or public interest technologists in Internet governance and standards bodies.
* To increase the diversity of representation in these bodies.
           
## 2. Fund-award Duration

2.1 This MOU is effective upon both parties’ signing and will be extend until the report obligations from the Recipient are fully executed after participation in the concerned event or funds are fully returned to Derechos Digitales, in any other case.

## 3. Tasks and Responsibilities of Recipient

3.1 Goals:

* The Travel-Fund Recipient will be working on public interest technology issues including, but not limited to, Security and Privacy, Censorship circumvention, Access and Community Networks, Algorithmic Decision-Making, Business and Human Rights and Public Interest Internet Standards.
* The Recipient will apply their skills and experience toward completing their concrete outputs, as well as toward fulfilling the Goals of the Fund-award. This request should specify the items of expenses coverage requested: registration, train/airfare, accommodation, taxis, visa fees, travel insurance, meals and child care.
      
3.2 Narrative Report:

* Within 15 days after participation in the awarded event, the Recipient will submit a narrative report on how was their experience participating in the event and how it impacted their work, including observations on things learned, connections made, possibilities for future actions, or any other relevant elements from their experience. Consider also mentioning challenges faced that can be relevant for PITG-Fund to know about.

3.3 Expenses Report:

* Within 15 days after participation in the awarded event, and in the same document prepared for the narrative report, the Recipient will include a general summary of the expenditures covered by the Fund-award, whether the expense has been made by the Recipient directly with money handed from the Fund, or has been coordinated with the Recipient and executed by the Host.

Whenever it is possible, the Recipient will add to the financial report:
	- receipts and boarding passes (for travel airfare/trains/buses) to the event destination containing names of all guests paid,  when such travel has not been coordinated by the Host.
	- original receipts for accommodation/hotel containing names of all guests paid for, as well as the duration of their stay, only when accommodations have not been coordinated by the Host.
	
	- The Fund-award will only reimburse living expenses (local transport, meals, incidental costs, etc.) up to 70% of the per diem rate for meals and incidental costs set for the location by the US State Department. Expenses up to this daily amount do not need proof of payment: the Recipient will just provide a signed invoice from the Recipient stating the delivery of the money. 

	- Exceptions will be made to these expenditure rules to accommodate for disabilities and/or childcare needs. Please ensure that you note such a requirement explicitly in your proposal.

Any associated reimbursements stemming from the expenses report must not be in excess of 5% from the amount the Recipient proposed in their application. If the reimcursemsnts exceed the 5% mark, the Recipient will provide a note with reasons thereof. The Host Organization reserves the right to reject any reimbursement that exceeds the initially proposed amount.

All expenses made in local currency, must be converted to USD using https://www1.oanda.com/currency/converter/

## 4. Tasks and Responsibilities of Host Organization

4.1 Policies & Procedures: The Host Organization is responsible for informing the Recipient of any relevant policies and procedures in relation to their travel and stipend provided by the PITG-Fund sub-group. All of this policies and procedures are mentioned in this agreement, and the Recipient agrees to know and accept them by signing. Both parties will work together to manage any issues as they arise.

4.2 Reports Processing: The Host Organization will be responsible for requiring, receiving and analysing both narrative and financial reports from the Recipient. Host Organization will review and observe any issues that need additional information or clarification directly with the Recipient, and will inform the results to the PITG-Fund sub-group.

## 5. Budget and Funds

5.1 Travel: All funds requested and approved will be handed to the Recipient by the Host Organization in the most expeditious way possible, avoiding unnecessary bank charges. Travel expenditures such as airfares and accommodations, as well as perdiem and other funds required and approved will be coordinated and executed by the Recipient directly using the transferred funds. In some specific cases, and for previously justified reasons, the Host Organization may take charge directly of the management of any of the required expenses, upon receipt of specific request of dates and route for the traveling as soon the award is informed to them, or in any case with at least 1 month in advance to the event. That information will be sent to the Host organization contact person identified in this MOU. The Recipient will administer the funding to cover travel expenses in a reasonably and efficient way, trying to obtain the most convenient fares and making the necessary reservations with sufficient advance for such purposes.

5.2 Returning of funds: The Grantee is subject to returning the provided award if:

* There's failure to secure a visa after the applicant's expressed agreement of having considered visa/travel restrictions in the application form.

* There's failure to catch the flight/attend the event under foreseeable circumstances.

* In the case of cancellation of the awarded event.

Costs caused by any of the described above could be charged to the Recipient if there is responsibility from them in the failure. If no responsibility in the failure is applicable, the extra costs could be deducted from the project's funds.

5.3 Employment status: This MOU does not constitute an employment contract. The Host Organization is therefore not responsible for payment of employee or fringe benefits, including but not limited to medical coverage, life or disability insurance, workers compensation insurance, unemployment insurance, or other taxes (including penalties and interest).

## 6. Termination

6.1 Conflict of Interest: Both parties will take all necessary measures to avoid conflicts of interest and shall inform each other immediately of any situation giving rise to or likely to give rise to any such conflict.

6.2 Immediate termination: The Host Organization in behalf of the PITG-Fund may terminate a Fund-award at any point for reasonable cause, such as but not limited to: failure to comply with the terms of this MOU; violation of applicable policies or procedures; violation of a law of the country of assignment; failure to comply with PITG Code of Conduct; repeated failure to respond to requests or instructions from the Host; material misrepresentation in the Fund-award application or other Fund-award documents, or engagement in conduct that brings the Host Organization or the PITG-Fund into disrepute or is inappropriate or dangerous to others. Both parties agree to make reasonable attempts to problem-solve in good faith before ending the Fund-award, and the Host Organization agrees to notify the Recipient of any deficiency and seek to provide avenues for correction before resorting to termination, when possible.

6.3 Cancellation of event: Notice: The Fund-award may be terminated at any time by the Host by giving written notice to the Recipient in the case of cancellation of the awarded event.

Signed:

**Host Organization**

- Signature:
On behalf of Derechos Digitales PITG-Fund manager

- Print Name:
- Date:
- Contact:

**Recipient**

- Signature:
- Print Name:
- Date:
