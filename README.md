# PITG Fund

This is the space for applicants and organizers of the PITG Fund.

## Volunteer

The [funding policy](https://gitlab.com/pitg/fund/-/blob/master/FundingPolicy.md) is set and funding decisions are allocated by a group of volunteers.
To partipcate as a volunteer please join the PITG Fund [subgroup's mailing list](https://lists.mayfirst.org/mailman/listinfo/pitg-fund).

## Apply 

To apply for the travel fund, please read the [funding policy](https://gitlab.com/pitg/fund/-/blob/master/FundingPolicy.md) and send an email to pitg-fund@lists.mayfirst.org and include the following information:

 * Recipient information (Name, organization, email, phone, address):
 * Event information (IETF/W3C/ITU/IEEE/3GPP/ICANN/et al, date):
 * Whether or not you are pre-approved or accredited for the meeting?:
 * Financial assistance required, by item:
    - __ Registration fee
    - __ Train/airfare
    - __ Accommodation
    - __ Taxis
    - __ Visa fees
    - __ Travel insurance
    - __ Meals
    - __ Child care
    - **Total amount ____**

Please apply at least two months in advance of the date of the event you are proposing to attend.

The PITG Fund group will confirm receipt of your email. Within two weeks of your sending all the information listed above, the Fund group will reply to approve or decline your application.

## Required documents (for reference)

Once your application has been approved, you must:

* Send your [financial information](https://gitlab.com/pitg/fund/-/blob/master/FinancialInformation.md)

* Comply with the [PITG Code of Conduct](https://gitlab.com/pitg/code-of-conduct/-/blob/master/code-of-conduct.md)

* Accept and sign a [Memorandum of Understanding (MOU)](https://gitlab.com/pitg/fund/-/blob/master/MOU.md) for recipients

## Other sources for travel funding

 * [IRTF travel fund](https://irtf.org/travelgrants)
 * [IETF LLC travel fund]()
 * [ISOC]()
 * [Open Technology Fund's guide to alternative funding sources](https://guide.opentech.fund/appendix-iv-alternative-sources-of-support)
 * [Geek Feminism Wiki's travel funding page](https://geekfeminism.fandom.com/wiki/Travel_funding)
 * [Funding for individual and organizational members of the Association for Progressive Communications](https://www.apc.org/en/project/member-exchange-and-travel-fund)
